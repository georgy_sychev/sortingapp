package org.sortingApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static Logger LOGGER = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args){
        LOGGER.info("Logging info from Main");

        String[] array = {"54", "43", "546", "15", "-5","-99"};
        SortingApp sortingApp = new SortingApp();
        sortingApp.sort(array);

        System.out.println(String.join(" ", array));

    }
}