/**
 * @author George Sychev
 */
package org.sortingApp;
import java.util.Arrays;

public class SortingApp {

    /**
     * The method sorts the array
     * @param inputArr array of strings between 1 and 10 values
     * @throws IllegalArgumentException if null or out of range
     */
    public void sort(String[] inputArr) throws IllegalArgumentException{
        if (inputArr == null || inputArr.length < 1 || inputArr.length > 10 ){
            throw new IllegalArgumentException("Can't be null, less than 1 or bigger than 10");
        }

        int[] intArrToSort = new int[inputArr.length];

        for (int i =0; i < inputArr.length; i++) {
           intArrToSort[i] = tryParseInt(inputArr[i]);
        }

        Arrays.sort(intArrToSort);

        for (int i =0; i < intArrToSort.length; i++) {
            inputArr[i] = String.valueOf(intArrToSort[i]);
        }
    }
    /**
     * The method tries to convert a string to a number,
     * @throws IllegalArgumentException error or
     * @return an integer
     */
    private static int tryParseInt(String value){
        try {
            return Integer.parseInt(value);
        }catch (NumberFormatException exception){
           throw new IllegalArgumentException("Wrong input");

        }
    }
}
