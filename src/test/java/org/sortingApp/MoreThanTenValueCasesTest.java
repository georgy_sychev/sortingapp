package org.sortingApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
@RunWith(Parameterized.class)
public class MoreThanTenValueCasesTest {
    private SortingApp sortingApp = new SortingApp();

    private String[] input;

    public MoreThanTenValueCasesTest(String[] input){
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> params() {
        return Arrays.asList(new Object[][]{
            { new String[] { "1", "2", "5", "4", "3", "7", "6", "8", "10", "9", "11"}},
            { new String[] { "1", "2", "5", "4", "3", "7", "6", "8", "10", "9", "11", "12"}},
            { new String[] { "1", "2", "5", "4", "3", "7", "6", "8", "10", "9", "11", "13", "12"}},
            { new String[] { "1", "2", "5", "4", "3", "7", "6", "8", "10", "9", "11", "13", "12", "14"}},
            { new String[] { "1", "2", "5", "4", "3", "7", "6", "8", "10", "9", "11", "13", "12", "15", "14"}},

        });
    }
    @Test(expected = IllegalArgumentException.class)
    public void moreThanTenArgumentCase(){
        sortingApp.sort(input);
    }
}
