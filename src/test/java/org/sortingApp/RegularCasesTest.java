package org.sortingApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class RegularCasesTest {
    private SortingApp sortingApp = new SortingApp();

    private String[] actual;
    private String[] expected;

    public RegularCasesTest(String[] actual, String[] expected){
        this.actual = actual;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> params() {
        return Arrays.asList(new Object[][]{
            { new String[] { "4"}, new String[] {"4"}},
            { new String[] { "22", "4", "7"}, new String[] {"4", "7","22"}},
            { new String[] { "1", "2", "5", "4"}, new String[] {"1", "2", "4", "5"}},
            { new String[] { "5", "-4", "-8", "6"}, new String[] {"-8", "-4", "5", "6"}},
            { new String[] { "1", "2", "5", "4","11", "8"}, new String[] {"1", "2", "4", "5", "8", "11"}},
            { new String[] { "1", "2", "5", "4","11", "8","15","14","12","13"}, new String[] {"1", "2", "4", "5", "8", "11","12","13","14","15"}},

        });
    }
    @Test
    public void regularCase(){
        sortingApp.sort(actual);
        assertArrayEquals(expected,actual);
    }
}
