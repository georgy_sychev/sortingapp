package org.sortingApp;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortingAppTest {
    private SortingApp sortingApp = new SortingApp();

    @Test(expected = IllegalArgumentException.class)
    public void emptyArrayCase() {
        String[] input = {};
        sortingApp.sort(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullArrayCase(){
        sortingApp.sort(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void moreThanTenValuesCase(){
        String[] input = {"5", "12", "-66", "-54", "11", "888", "15", "65", "22", "1", "0"};
        sortingApp.sort(input);
    }
    @Test(expected = IllegalArgumentException.class)
    public void wrongValueCase(){
        String[] input = {"5", "12", "-66", "-54", "ab"};
        sortingApp.sort(input);
    }
    @Test
    public void oneValueCase(){
        String[] input = {"5"};
        sortingApp.sort(input);
        assertEquals("5", input[0]);
    }
    @Test
    public void tenValueCase(){
        String[] input = {"5", "12", "-66", "-54", "11", "888", "15", "65", "22", "1"};
        sortingApp.sort(input);
        String[] correctResult = {"-66", "-54", "1", "5", "11", "12", "15", "22", "65", "888"};
        assertArrayEquals(correctResult, input);
    }


}