package org.sortingApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class WrongValueCasesTest {
    private SortingApp sortingApp = new SortingApp();

    private String[] input;

    public WrongValueCasesTest(String[] input){
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> params() {
        return Arrays.asList(new Object[][]{
            { new String[] { "1", "2", "5", "4","Hello"}},
            { new String[] { "OK", "hello", "example@g.com"}},
            { new String[] { "-541Gh", "gg42", "559i", "$$^4",}},
            { new String[] { "Go", "2", "Go", "4", "Go" }},
            { new String[] { "Go", "go", "$$Y__Y$$", "4", " "}},

        });
    }
    @Test(expected = IllegalArgumentException.class)
    public void wrongValueCase(){
        sortingApp.sort(input);
    }
}
